/* Richard Pingle
	Configuration Management Assignment
	CIT 337
	This code is the accompanying "Beer Song"
   for the Configuration Management Assignment
	*/
	
public class BeerSong
{
	 // class method definition setion
	public static void main(String args[])
	{
		Nintey_Nine_Bottles_of_Beer(99);	
	}//end main
	
public static void Nintey_Nine_Bottles_of_Beer(int b)
	{
		if(b>=0)
		{
			if(b>1)
			System.out.println(b+" bottles of beer on the wall\n"+b+" bottles of beer\nTake one down, pass it around\n"+(b-1)+" bottles of beer on the wall.\n");	
			else if(b==1)
			System.out.println(b+" bottle of beer on the wall\n"+b+" bottle of beer\nTake one down, pass it around\n"+(b-1)+" bottles of beer on the wall.\n");
			else
			System.out.println(b+" bottles of beer on the wall\n"+b+" bottles of beer\nBetter go to the store and buy some more!");
			Nintey_Nine_Bottles_of_Beer(b-1);
		}	
	}

}//end of class